json.array!(@people) do |person|
  json.extract! person, :id, :name, :weight, :height, :color, :education, :nationality
  json.url person_url(person, format: :json)
end
